﻿def test_http_status_code_is_ok(http_status_code):
    """Checks if response status code is HTTP OK 200"""
    assert http_status_code == 200


def test_short_id_matches_id(json_content):
    """Checks if beginning of id matches short id"""
    assert json_content["id"][:8] == json_content["short_id"]


def test_stats_is_not_empty(json_content):
    """Checks if JSON data contains stats item"""
    assert json_content["stats"]
